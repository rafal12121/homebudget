class TransactionsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @transactions = Transaction.all
  end

  def edit
    @transaction = Transaction.find(params[:id])
  end
  
  def show
    @transaction = Transaction.find(params[:id])
  end

  def create
    @transaction = @current_user.transactions.build(transaction_params)
    @transaction.save
    redirect_to transactions_path, :notice => 'Transaction added!'
  end

  def update
    @transaction = @current_user.transactions.find(params[:id])
    if @transaction.update_attributes(transaction_params)
      redirect_to transactions_path
    else
      redirect_to transactions_path
    end
  end

  def destroy
    @current_user.transactions.find(params[:id]).destroy
    redirect_to transactions_path, :notice => 'Transaction deleted!'
  end

private

def transaction_params
  params.require(:transaction).permit(:name, :value)
end

end