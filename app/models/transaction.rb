class Transaction
  include Mongoid::Document
  include Mongoid::Timestamps

  field :id, type: String
  field :name, type: String
  field :value, type: Float, default: 0

  validates_presence_of :name
  # run 'rake db:mongoid:create_indexes' to create indexes

  #belongs_to :user
  embedded_in :user, inverse_of: :transactions

end