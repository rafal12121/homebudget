class User
  include Mongoid::Document
  include Mongoid::Timestamps
  rolify
  field :provider, type: String
  field :uid, type: String
  field :name, type: String
  field :email, type: String


  validates_presence_of :name
  # run 'rake db:mongoid:create_indexes' to create indexes

  #has_many :transactions
  embeds_many :transactions


  index({ email: 1 }, { unique: true, background: true })

  def self.create_with_omniauth(auth)
    create! do |user|
      user.provider = auth['provider']
      user.uid = auth['uid']
      if auth['info']
         user.name = auth['info']['name'] || ""
         user.email = auth['info']['email'] || ""
      end
    end
  end

end
