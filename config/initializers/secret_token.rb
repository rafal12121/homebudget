# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
HomeBudget::Application.config.secret_key_base = '3f94a8b41773fb9888d5798c1138902d6a196b6bb601a0970974f30d5a5b49268877aa195dd5f675c545a15f679e463c89ecbd11d3a5ffaa567b7ceb444587a9'
