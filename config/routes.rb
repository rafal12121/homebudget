HomeBudget::Application.routes.draw do
  root :to => "transactions#index"
  resources :users, :only => [:index, :show, :edit, :update ]
  resources :transactions
  get '/auth/:provider/callback' => 'sessions#create'
  get '/signin' => 'sessions#new', :as => :signin
  get '/signout' => 'sessions#destroy', :as => :signout
  get '/auth/failure' => 'sessions#failure'
end
